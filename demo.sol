// SPDX-License-Indetifier: MIT

pragma solidity ^0.8.0;
contract Shop{
    address owner; 
    mapping (address => uint) public payments; // хранение адреса покупателя 
    constructor(){
        owner = msg.sender; //получение адреса владельца контракта, данные хронятся в блокчейне 

    }

    function pay()public payable  { // для функции которая принимает деньги можно не описывать тело 
        payments[msg.sender] = msg.value; 
    }

    function withdrawAll() public{
        address payable _to = payable(owner);
        address _thisContract = address(this);
        _to.transfer(_thisContract.balance);
    }
}
