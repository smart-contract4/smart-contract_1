pragma solidity ^0.8.9; 
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";


contract TestNft is ERC721Enumerable, Ownable{
    uint public constant PRICE = 0.005 ether;
    string public baseTockenURI;
    uint[] soldedTokensIds; ---- new
    mapping(addres => uint[])nftOwner;
    event MintNft(address senderAddress, uint_tokenId);

    
}
constructor(string memory baseURI) ERC217("My nft","NFT"){
    setBaseURI(baseURI);
}

function _baseURI() internal view virtual override returns(string memory){
    return baseTockenURI;
}

function reserveNFT(uint _tokenId) public onlyOwner{
    _safeMint(msg.sender, _tokenId);
    nftOwner[msg.sender].push(_tokenId);
    soldedTokensIds.push(_tokenId); ---- new
    emit MintNft(msg.sender, _tokenId);
}

function mintNFT(uint _tokenId) pablic payable{
    require(msg.value >= PRICE, "Not enough ether to purchase NFT.")
    _safeMint(msg.sender, _tokenId);
    soldedTokensIds(_tokenId); ---- new 
    emit MintNft(msg.sender, _tokenId);
}

function tokensofOwer(address _owner) external view returns{
    return nftOwner[_owner];
}

function withdraw() public payable onlyOwner{
    uint balance = address(this).balance;
    require(balance > 0, "No ether left to withdarw");
    (bool succes, ) = (msg.sender).call{value: balance}("");
    require(succes, "Transfer failed.");
}

modifier checkTokenStatus(uint_tokenId){
    bool isTokenSold = false; 
    for(uint i = 0; i<soledTokenIds.length;i++){
        if(soledTokenIds[i] == _tokenId){
            isTokenSold = true;
            break; 
        }
    }
    require(!isTokenSold, "Token is sold ");
    _;
}
